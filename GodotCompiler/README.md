# Godot Compiler

This is a tool for automating the build process of Godot itself.

If you would like to modify Godot's source code or use Modules in addition to
Godot's Core (like [Zylann's Voxel](https://github.com/Zylann/godot_voxel)),
than this is the place for you. Otherwise, here be dragons.

# How to use

To use this, you will be required to modify the `.gitlab.yml` file that ships
with this project. I will be using Zylann's Voxel as an example.

```yml
image: greenfox1505/godot-ci-builder

stages:
  - compile
  - build
  - export

compile:
  image: greenfox1505/godot-ci-compiler #this image will be merged back to core
  stage: compile
  script:
  - mkdir -p temp; cd temp
  - getGodot "https://github.com/godotengine/godot.git" "3.1.2-stable"
  - getModule "https://github.com/Zylann/godot_voxel.git" "godot3.1" "voxel"
  - compileGodot core #required
#   - compileGodot windows
  - compileGodot linux
  cache:
    paths:
    - temp
  artifacts:
    paths:
    - bin

build: #keep this from before
  stage: build
  script:
  - mv bin/godot_server.x11.opt.tools.64 /usr/local/bin/godot
  - builder
  artifacts:
    paths:
    - bin/*
    - public/*
  only:
  - master

#tool commands
#getGodot "https://GitRepo.com/godot.git" "BranchName"
#getModule "https://GitRepo.com/Module.git" "BranchName" "LinkTarget"
#compileGodot [core,windows,linux] #core must be called for compile to work. others must be called for each target platform
```




# Keeping clean build caches

mkdir for platform
git clones Godot, checkout branch
git clones module, checkout branch
copy bin to output dir



# Afterthought

I feel like building from a core baseline should be easier. I've been trying to
make my build style fit GitLab's architecture, but maybe I just need to build a
single thing that does what it should.